/**
 * Created by jarek_000 on 04.03.2016.
 */
import NotFound from './pages/NotFound';
import Error from './pages/Error';
import MainHome from './pages/MainHome';

export default {Error, NotFound,MainHome};
