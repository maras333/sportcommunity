import React, { Component, PropTypes } from 'react';
import Link from '../Link';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import CreateEventForm from '../CreateEventForm/CreateEventForm.js';
import ImageServices from '../../../../services/ImageServices/imageServices.js';
import EventServices from '../../../../services/EventServices/EventServices.js';


import runningMan from '../Header/running-man_transparent.png';


class AppBarExampleIconMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: 0,
      error: 0,
      message: 0,
      openDialog: false,
      linkHover: false,
      files: props.user.images,
      img: {},
      userId: {
        id: props.user.id,
      }
    }
    console.log(this.state.userId);
  }

  handleLogout() {
    this.props.userLogout(() => {
      Location.push('/sportcommunity/');
    });
  }

  toggleHover = () => {
    this.setState({linkHover: !this.state.linkHover})
  };


  handleOpenDialog = () => {
    this.setState({openDialog: true});

  };

  handleCloseDialog = () => {
    this.setState({openDialog: false});
  };

  async handleFile(e) {
    var reader = new FileReader();
    var file = await e.target.files[0];
    var data = new FormData();
    await data.append('file', file);
    var respond = await fetch('/api/images/', {
      method: 'post',
      body: data,
      credentials: 'include'
    });

    console.log(this.props);

    var file2 = await respond.json();
    console.log(file2);
    this.setState({
      img: file2,
    });
  }

  handleChange(e) {
    var imgObj = this.state.img;
    imgObj[e.target.name] = e.target.value;
    this.setState({img: imgObj});

    console.log(this.state.img);
  }

  onSubmit(event) {
    console.log('onSUBMIT');
    this.setState({ error: 0 });
    this.setState({ success: 0 });

    event.preventDefault();

    if (this.state.activeApi) {
      return 0;
    }

    this.setState({ activeApi: true });
    (async () => {
      try {
      this.setState({
        userId: {id: this.props.user.id}
      });
      var message = await ImageServices.update(this.state.img, this.state.userId);
      var result = message.json();
      console.log(result);
    } catch (e) {
        this.setState({ error: 1, success: 0 });
    }
    this.setState({ activeApi: false });

    this.setState({openDialog: false});
    this.setState({img: {}});
    })();


  }

  render() {
  if (this.props.user) {
    var linkToUser=`/sportcommunity/user/${this.props.user.id}`;
  }
  // toggleHover
  var linkStyle;
  if (this.state.linkHover) {
    linkStyle = {color: '#4d4949', fontSize:'1.5em',}
  } else {
    linkStyle = {color: 'white', fontSize:'1.5em',}
  }

    let menuItems = (
      <div>
        <Link to="/sportcommunity/users/">
          <MenuItem primaryText="Lista uzytkownikow"></MenuItem>
        </Link>
        <Link to="/sportcommunity/information/">
          <MenuItem primaryText="Info"></MenuItem>
        </Link>
        {this.props.user ?
            <MenuItem primaryText="Log out" href="/api/user/logout"></MenuItem>
          :
          <Link to="/sportcommunity/login/">
            <MenuItem primaryText="Log in"></MenuItem>
          </Link>
        }
      </div>
    );

    const actionsDialog = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleCloseDialog}
      />,

    ];

    const styles = {
      float: 'left',
    }

    return(
      <AppBar
        title="SportCommunity"
        iconElementLeft={
          <Link to="/sportcommunity/">
            <img src={runningMan} height="50" width="50"></img>
          </Link>
        }
        iconElementRight={
          <div>
            {this.props.user ?
              <div style={{float: 'left', margin: '0 auto', paddingRight: '4em', paddingTop: '5px'}}>
                <RaisedButton label="New event" onTouchTap={this.handleOpenDialog} />
                <Dialog
                  title="Create new event"
                  modal={true}
                  actions={actionsDialog}
                  open={this.state.openDialog}
                  autoScrollBodyContent={true}
                  autoDetectWindowHeight={false}
                >
                  <div className="row" >
                    <div className="col-sm-offset-2 col-xs-8">
                      <form  onSubmit={this.onSubmit.bind(this)}>
                        <CreateEventForm img={this.state.img} handleFile={this.handleFile.bind(this)} handleChange={this.handleChange.bind(this)} />
                        <RaisedButton  type="submit" label="Zapisz" fullWidth={true} primary={true} />
                      </form>
                    </div>
                  </div>
                </Dialog>
              </div> : ""
            }
            <div style={{float: 'left', margin: '0 auto', paddingRight: '6em', paddingTop: '7px'}}>
                {this.props.user ?
                  <Link to={linkToUser} > <span style={linkStyle} onMouseEnter={this.toggleHover} onMouseLeave={this.toggleHover}>{this.props.user.email}</span></Link>
                    :
                    ""
                }
              </div>
              <IconMenu
                iconButtonElement={
                  <IconButton><MoreVertIcon /></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >
                {menuItems}
              </IconMenu>

          </div>
        }
      >
      </AppBar>
    );
  }
}

export default AppBarExampleIconMenu;
