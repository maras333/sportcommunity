/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';


class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: this.props.value
    }
  }

  onChange(e) {
   this.props.onChange(!this.state.checked);

    this.setState({
      checked: !this.state.checked
    })
  }
  render() {

    return (
      <div className="checkbox-wrapper">
        <label className="checkbox-label-sm">
          {this.state.checked ?
            <input type="checkbox" checked name={this.props.name} value={this.props.value} onChange={this.onChange.bind(this) } />
            :
            <input type="checkbox" name={this.props.name} value={this.props.value} onChange={this.onChange.bind(this) } />
          }
          <div className="checkbox-sm"></div>
          {this.props.label }
        </label>
      </div>
    )
  }

}

export default Checkbox;
