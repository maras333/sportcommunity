/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

class Content extends Component {

  render() {

    return (
      <div className="Content clearfix">
        <div className="content-container col-md-12 col-xs-12 col-sm-12">
          <div className="content-car-logo visible-xs"></div>
          <div className="content-car"></div>
          <div className="content-description">
            <p className="content-car-description">
              Da voloreicia as quae nemodior alit magni consers pedit, officitat as autet eum quamusc ilisto od ut volupta tusaperis evellist, ipidebitat earuptus eos alique ium dent aperate nimiliquias aspernat lab inihic tem ese sitemquis aligenem nis et et reprovit pratatu saeribus ditat facestissit, il isquas et ex ex expedi se cum restium hit ea perupta ereria deraest iasit, odi aped eum lia vent, ius quatur? Ut mo enduci conserorem esti ut vitaecto tectur sint as quidebis ad es aruptas del eostio officit ut incius.
            </p>
          </div>
        </div>
      </div>
    )
  }

}

export default Content;
