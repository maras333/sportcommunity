import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';


class TextFieldExampleSimple extends Component {

  constructor(props) {
    super(props);
  }

  handleFile(event) {
    this.props.handleFile(event);
  }

  handleChange(event) {
    this.props.handleChange(event);
  }

  render() {
    console.log(this.props.img);
    let styles = {
      exampleImageInput: {
        cursor: 'pointer',
        position: 'absolute',
        top: '0',
        bottom: '0',
        right: '0',
        left: '0',
        width: '100%',
        opacity: '0'
      }
    }
    return (
      <div>
        <div style={{textAlign: 'center'}}>
          <FlatButton label="Upload photo" primary={true}>
            <input
              name="file"
              type="file"
              accept=".png,.jpg,.jpeg"
              style={styles.exampleImageInput}
              onChange={this.handleFile.bind(this)}
            />
          </FlatButton>
        </div>
        <TextField
          fullWidth={true}
          floatingLabelText="Name of event"
          name="name"
          value={this.props.img['name']}
          onChange={this.handleChange.bind(this)}
        /><br />
        <TextField
          fullWidth={true}
          floatingLabelText="Caption"
          multiLine={true}
          rows={1}
          rowsMax={3}
          name="caption"
          value={this.props.img['caption']}
          onChange={this.handleChange.bind(this)}
        /><br />
        <TextField
          fullWidth={true}
          floatingLabelText="Your Time"
          name="time"
          value={this.props.img['time']}
          onChange={this.handleChange.bind(this)}
        />
        <div>
        {this.props.img.url ?
        <CardMedia
          overlay={<CardTitle title="New event photo" />}
        >
          <img src={this.props.img.url} />
        </CardMedia> : ""
        }
        </div>
      </div>
    );
  }
}

export default TextFieldExampleSimple;
