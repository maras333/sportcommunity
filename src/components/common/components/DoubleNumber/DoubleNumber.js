/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';


class DoubleNumber extends Component {
  handleChangeMin(event) {
    this.props.onChangeMin(event.target.value);
  }
  handleChangeMax(event) {
    this.props.onChangeMax(event.target.value);
  }

  render() {

    return (
      <div class="double-select">
        <div className="form-group form-inline form-group-color">
          <label for="" className="double-label">{this.props.label}</label>
          <input type="number" value={this.props.value.min} placeholder="od" className="form-control double-form form-control-custom" onChange={this.handleChangeMin.bind(this)} id="" />
          <input type="number" value={this.props.value.max} placeholder="do" className="form-control double-form form-control-custom" onChange={this.handleChangeMax.bind(this)} id="" />
        </div>
      </div>
    )
  }

}

export default DoubleNumber;
