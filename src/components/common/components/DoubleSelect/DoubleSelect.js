/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';


class DoubleSelect extends Component {

  render() {

    return (
      <div class="double-select">
        <div className="form-group form-inline form-group-color">
          <label for="" className="double-label">{this.props.label}</label>
          <select className="form-control double-form form-control-custom" id=""></select>
          <select className="form-control double-form form-control-custom" id=""></select>
        </div>
      </div>
    )
  }

}

export default DoubleSelect;
