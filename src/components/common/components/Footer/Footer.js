/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

class Footer extends Component {

  render() {

    return (
      <div className="Footer">
        <div className="custom-footer">
          <div className="custom-footer-gray">
          </div>
          <div className="custom-footer-dark">
            <p className="footer-text">
              Wszelkie prawa zastrzeżone Marek Czyż
            </p>
          </div>
        </div>
      </div>
    )
  }

}

export default Footer;
