/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';


class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = { login: {}, errors: {} };
  }

  onSubmit(e) {
    e.preventDefault();
    if (!this.checkForm()) {
      return false;
    }
    const data = JSON.stringify(this.state.login);
    const options = {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: data,
      credentials: 'include'
    };
    if (this.props.pendingLogin) {
      this.props.pendingLogin();
    }
    (async () => {
      try {
        const result = await fetch('/login/local', options);
        const message = await result.json();
        if (message.error) {
          this.setState({ error: 1, message: message.error.map((error) => error.message) });
          if (this.props.errorLogin) {
            this.props.errorLogin();
          }
        } else {
          if (message.success) {
            this.setState({ success: 1 });
            if (this.props.successLogin) {
              this.props.successLogin();
            }
          } else {
            this.setState({ error: 1, message: 'Niepoprawne dane' });
            if (this.props.errorLogin) {
              this.props.errorLogin();
            }
          }

        }
      } catch (error) {
        console.log(error);
        if (this.props.errorLogin) {
          this.props.errorLogin();
        }
        this.setState({ error: 1 });
      }
    })();
    return false;
  }

  checkForm() {
    var error = false;
    var required = ['username', 'password']
    for (var el of required) {
      console.log(el, this.state.login[el]);
      if (!this.state.login[el]) {
        this.state.errors[el] = "Pole obowiązkowe"
        error = true;
      }
    }

    this.setState({
      errors: this.state.errors
    })

    return !error;
  }


  handleChange(event) {
    let obj = this.state.login;
    obj[event.target.name] = event.target.value;
    this.state.errors[event.target.name] = false;
    this.setState({ login: obj, errors: this.state.errors });
  }

  render() {

    return (
      <div className="login-form">
        <h2 className="clear-bs">Zaloguj się</h2>
        <div className="col-md-6 col-sm-6 clear-bs">
          <form  action="/login/local" method="POST" onSubmit={this.onSubmit.bind(this) }>
            <div className="auth-form-label">
              <label for="" className="">E-mail</label>
              <input type="email" className="auth-input auth-login-input" id="" placeholder="" name="username" value={this.state.login.username} onChange={this.handleChange.bind(this) }/>
              <div className="auth-form-error">{this.state.errors.username ? this.state.errors.username : ""}</div>
            </div>
            <div className="auth-form-label">
              <label for="" className="">Hasło</label>
              <input type="password" className="auth-input auth-login-input" id="exampleInputPassword1" name="password" placeholder=""  value={this.state.login.password} onChange={this.handleChange.bind(this) }/>
              <div className="auth-form-error">{this.state.errors.password ? this.state.errors.password : ""}</div>
            </div>
            <div className="">
              <label>
                <input type="checkbox" className=
                  "auth-checkbox"/> <span className="remember-me">Zapamiętaj mnie</span>
              </label>
            </div>
            <button type="submit" className="auth-btn auth-login-btn">Zaloguj się</button>
          </form>
        </div>
        <div className="col-md-6 col-sm-6 clear-bs">
          <a href="" className="password-forget">Zapomniałeś hasła?</a>
        </div>

      </div>
    )
  }

}

export default LoginForm;
