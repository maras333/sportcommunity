/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
//import s from './Navigation.scss';
import Link from '../Link';
import AppBar from 'material-ui/AppBar';


import MyAppBar from '../AppBarIconMenu/AppBarIconMenu.js'
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';


import ss from '../Header/Header.scss';
import logoUrl from '../Header/logo-small.png';
import runningMan from '../Header/running-man_transparent.png';

class Navigation extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
          <MyAppBar user={this.props.user}/>
      </div>
    );
  }
}

export default Navigation;
