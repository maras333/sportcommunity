/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';


class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = { register: {}, errors: {}, error: false };
  }
  onSubmit(e) {
    e.preventDefault();
    if (!this.checkForm()) {
      return false;
    }
    const data = JSON.stringify(this.state.register);
    const options = {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: data,
      credentials: 'include'

    };
    if (this.props.pendingLogin) {
      this.props.pendingLogin();
    }
    (async () => {
      try {
        const result = await fetch('/api/register', options);
        const message = await result.json();
        if (message.error) {
          this.props.errorLogin();
          this.setState({ error: 1, message: message.error.map((error) => error.message) });
        } else {
          this.props.successLogin();
          this.setState({ success: 1 });

        }
      } catch (error) {
        if (this.props.errorLogin()) {
          this.props.errorLogin();
        }

        this.setState({ error: 1, message: message.error.map((error) => error.message) });
      }
    })();
  }
  checkForm() {
    var error = false;
    var required = ['email', 'password', 'agreement', 'password2']
    for (var el of required) {
      console.log(el, this.state.register[el]);
      if (!this.state.register[el]) {
        this.state.errors[el] = "Pole obowiązkowe"
        error = true;
      }
    }
    if (this.state.register['password'] != this.state.register['password2']) {
      this.state.errors['password2'] = "Pole obowiązkowe"
      error = true;
    }
    this.setState({
      errors: this.state.errors
    })

    return !error;
  }
  verifyCallback(response) {
    console.log(response);
  }
  handleChange(event) {
    let obj = this.state.register;
    obj[event.target.name] = event.target.value;
    this.state.errors[event.target.name] = false;
    this.setState({ register: obj, errors: this.state.errors });
  }

  handleChangeCheckbox(event) {
    let obj = this.state.register;
    obj[event.target.name] = !obj[event.target.name];
    this.state.errors[event.target.name] = false;
    this.setState({ register: obj, errors: this.state.errors });
  }
  render() {

    return (
      <div className="signup-form">
        <h2 className="clear-bs">Utwórz konto</h2>
        <div className="col-md-6 col-sm-6 clear-bs">
          <form onSubmit={this.onSubmit.bind(this) } method="post" action="/api/register">
            <div className="auth-form-label">
              <label for="" className="">E-mail</label>
              <input type="email" className="auth-input auth-signup-input" id="" placeholder="" value={this.state.register.email} onChange={this.handleChange.bind(this) }  name="email"/>
              <div className="auth-form-error">{this.state.errors.email ? this.state.errors.email : ""}</div>
            </div>
            <div className="auth-form-label">
              <label for="" className="">Hasło</label>
              <input type="password" className="auth-input auth-signup-input" id="exampleInputPassword1" placeholder="" value={this.state.register.password} onChange={this.handleChange.bind(this) }  name="password"/>
              <div className="auth-form-error">{this.state.errors.password ? this.state.errors.password : ""}</div>
            </div>
            <div className="auth-form-label">
              <label for="" className="">Powtórz hasło</label>
              <input type="password" className="auth-input auth-signup-input" id="exampleInputPassword1" placeholder="" value={this.state.register.password2} onChange={this.handleChange.bind(this) }  name="password2"/>
              <div className="auth-form-error">{this.state.errors.password2 ? this.state.errors.password2 : ""}</div>
            </div>
            <button type="submit" className="auth-btn auth-signup-btn">Utwórz konto</button>
          </form>
        </div>
        <div className="col-md-6 col-sm-6 clear-bs">
          <form >
            <div className="">
              <label className="consent">
                {this.state.agreement ?
                  <input type="checkbox" className=
                    "auth-checkbox auth-checkbox-consent"  name="agreement"  checkbox value={this.state.register.agreement} onChange={this.handleChangeCheckbox.bind(this) }/>
                  :
                  <input type="checkbox" className=
                    "auth-checkbox auth-checkbox-consent"  name="agreement"  value={this.state.register.agreement} onChange={this.handleChangeCheckbox.bind(this) }/>
                }
                <div className="consent-text">
                  Oświadczam, że zapoznałem się i akceptuję
                  Regulamin serwisu SportCommunity.
                  Wyrażam zgodę na przetwarzanie moich danych
                  osobowych przez SportCommunity
                  <div className="auth-form-error">{this.state.errors.agreement ? this.state.errors.agreement : ""}</div>
                </div>

              </label>
            </div>
          </form>
        </div>

      </div>
    )
  }

}

export default RegisterForm;
