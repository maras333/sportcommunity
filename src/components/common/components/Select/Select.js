/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';


class Select extends Component {

  handleChange(event) {
    this.props.onChange(event.target.value);
  }

  render() {
    let option = [];
    if (Array.isArray(this.props.options)) {
      for (let el of this.props.options) {
        option.push(
          (<option value={el.id}>{el.label}</option>)
        );
      }
    }

    return (
      <div class="select">
        <div className="form-group form-group-color">
          <label for="" className="single-label">{this.props.label}</label>
          <select className="form-control form-control-custom" id="" value={this.props.value} onChange={this.handleChange.bind(this) }>
            <option value="">Wszystkie</option>
            {option}
          </select>
        </div>
      </div>
    )
  }

}

export default Select;
