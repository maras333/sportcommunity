/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import ReactSlider from 'react-slick';


class Slider extends Component {

  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    };
    let images = [];
    for (let img of this.props.images) {
      images.push(
        <div>
          <div className="slide hidden-xs hidden-sm" style={{ backgroundImage: `url('${img.desktop}')` }} />
          <div className="slide visible-xs" style={{ backgroundImage: `url('${img.mobile}')` }} />
          <div className="slide visible-sm" style={{ backgroundImage: `url('${img.tablet}')` }} />
        </div>
      );
    }

    return (
      <div className="slider">
        <ReactSlider {...settings}>
          {images}
        </ReactSlider>
      </div>
    )
  }

}

export default Slider;
