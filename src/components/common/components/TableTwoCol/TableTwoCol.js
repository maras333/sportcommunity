/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import Location from '../../../../core/Location';
import cx from 'classnames';

class Row extends Component {
  render() {
    var tmp = [];
    if(Array.isArray(this.props.value)){
      for (var el of this.props.value) {
        tmp.push(<div>{el}</div>)
      }
    } else {
      tmp = this.props.value;
    }
    return (
      <div className="table-row col-xs-12">
        <div className="row">
          <div className="col-sm-6 col-xs-12 title">
            {this.props.title}
          </div>
          <div className="col-sm-6 col-xs-12 value">
            {tmp}
          </div>
        </div>
      </div>
    )
  }
}

class TableTwoCol extends Component {
  render() {
    let list = [];
    for (let obj of this.props.data) {
      list.push(<Row title={obj.title} value={obj.value}/>);
    }
    return (
      <div className="table-two-col-component row">
        <div className="col-xs-12">
          {list}
        </div>
      </div>

    )
  }

}

export default TableTwoCol;
