/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import Location from '../../../../core/Location';
import cx from 'classnames';


class TitleValue extends Component {

  render() {

    return (
      <div className="titleValue">
        <span className="title">
           {this.props.title}
        </span>
        <span className="value">
          {this.props.value}
        </span>
      </div>

    )
  }

}

export default TitleValue;
