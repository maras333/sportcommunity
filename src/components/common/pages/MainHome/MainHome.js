/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import theme from '../../../../styles/theme.scss';
import emptyFunction from 'fbjs/lib/emptyFunction';

const title = 'Error';

class MainHome extends Component {

  static propTypes = {
    context: PropTypes.shape({
      insertCss: PropTypes.func,
      onSetTitle: PropTypes.func,
      onSetImage: PropTypes.func,
      onSetMeta: PropTypes.func,
      onPageNotFound: PropTypes.func,
      onSetUser: PropTypes.func
    }),
    children: PropTypes.element.isRequired,
    error: PropTypes.object
  };

  static childContextTypes = {
    insertCss: PropTypes.func.isRequired,
    onSetTitle: PropTypes.func.isRequired,
    onSetImage: PropTypes.func.isRequired,
    onSetUser: PropTypes.func.isRequired,
    onSetMeta: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired
  };



  getChildContext() {
    const context = this.props.context;
    return {
      insertCss: context.insertCss || emptyFunction,
      onSetTitle: context.onSetTitle || emptyFunction,
      onSetImage: context.onSetImage || emptyFunction,
      onSetMeta: context.onSetMeta || emptyFunction,
      onPageNotFound: context.onPageNotFound || emptyFunction,
      onSetUser: context.onSetUser || emptyFunction

    };
  }

  componentWillMount() {
    const { insertCss } = this.props.context;
    insertCss(theme);
  }

  render() {
    let img = [
      { desktop: require('../../../../images/home/slider-home-1.png'), mobile: require('../../../../images/slider-1-mobile.png'), tablet: require('../../../../images/slider-1-tablet.png') },
      { desktop: require('../../../../images/home/slider-home-1.png'), mobile: require('../../../../images/slider-1-mobile.png'), tablet: require('../../../../images/slider-1-tablet.png') },
      { desktop: require('../../../../images/home/slider-home-1.png'), mobile: require('../../../../images/slider-1-mobile.png'), tablet: require('../../../../images/slider-1-tablet.png') }
    ];



    return (
      <div className="home">
        <p>Main Home</p>
      </div>
    );
  }

}

export default MainHome;
