/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import Link from '../../../../components/Link';

class Header extends Component {

  render() {

    return (
      <div className="Header">
        <nav className="main-navigation">
          <div className="clearfix">
            <div className="nav-container col-md-12 col-xs-12">

              <Link to="/"><div className="logo">
              </div></Link>
              <ul className="navbar-list hidden-xs hidden-sm">
                <li className="language-styles">Pl</li>
                <li className="path"></li>
              </ul>
              <div className="hamburger visible-xs hidden-sm"></div>

            </div>
            <div className="tablet-nav-container col-sm-12 visible-sm clearfix">
              <ul className="navbar-list">
                <li className="language-styles">Pl</li>
                <li className="path"></li>
              </ul>
            </div> {/* Visibible only for tablets */}

          </div>
        </nav>
      </div>
    )
  }

}

export default Header;
