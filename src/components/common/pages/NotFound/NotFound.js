/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './NotFound.scss';
import cx from "classnames";
import Link from './../../components/Link';

const title = 'Nie znaleziono takiej strony | 404 not found';

class NotFound extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.context.onSetTitle(title);
    this.context.onPageNotFound();
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.background}></div>
        <div className={cx("content", s.content)}>
            <div className={s.icon}></div>
            <h1>Błąd 404</h1>
            <div className={s.text}>
                <p>Strona której szukasz nie została odnaleziona. <br/>
                Jeżeli masz jakieś pytania, <Link to="/contact">skontaktuj się z nami</Link>.</p>
            </div>
            <Link to="/"><button>WRÓĆ DO STRONY GŁÓWNEJ</button></Link>
        </div>
      </div>
    );
  }

}

export default withStyles(NotFound, s);
