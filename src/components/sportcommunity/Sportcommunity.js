/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import emptyFunction from 'fbjs/lib/emptyFunction';
import theme from '../../styles/theme.scss';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import cx from 'classnames';

import fetch from './../../core/fetch';

import Header from '../common/components/Header/Header.js';
import Footer from '../common/components/Footer/Footer.js';

import Home from './pages/home';
import Info from './pages/info/Info.js';
import Auth from './pages/auth/Page.js';
import UserPage from './pages/user/Page.js';
import UserListPage from './pages/userList/ListPage.js';
import EditUserPage from './pages/editUser/Edit.js';


import UserService from '../../services/UserServices/UserServices.js';

class Sportcommunity extends Component {

  static propTypes = {
    context: PropTypes.shape({
      insertCss: PropTypes.func,
      onSetTitle: PropTypes.func,
      onSetImage: PropTypes.func,
      onSetMeta: PropTypes.func,
      onPageNotFound: PropTypes.func,
      onSetUser: PropTypes.func
    }),
    children: PropTypes.element.isRequired,
    error: PropTypes.object
  };

  static childContextTypes = {
    insertCss: PropTypes.func.isRequired,
    onSetTitle: PropTypes.func.isRequired,
    onSetImage: PropTypes.func.isRequired,
    onSetUser: PropTypes.func.isRequired,
    onSetMeta: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      user: props.context.user && !props.context.user.error ? props.context.user : false,
      options: {},
    };
    console.log('sportcommunity.js');
  }

  static changePage() {
    if (typeof window != 'undefined') {
      var event = new Event('changePage');
      window.dispatchEvent(event);
    }
  }

  static registerRouter(on) {
    on('/sportcommunity/*', async (state, next) => {
      const component = await next();
      return component && <Sportcommunity context={state.context}>{component}</Sportcommunity>;
    });

    on('/sportcommunity/', async () => {
      Sportcommunity.changePage();
      return <Home />;
    });

    on('/sportcommunity/information', async () => {
      Sportcommunity.changePage();
      return <Info />;
    });

    on('/sportcommunity/login', async () => {
      Sportcommunity.changePage();
      return <Auth />;
    });

    on('/sportcommunity/users', async (state) => {
      Sportcommunity.changePage();
      var data = await UserService.getList();
      console.log(data);
      if(data) {
        return <UserListPage data={data}/>;
      } else {
        return
      }
    });

    on('/sportcommunity/user/:id', async (state) => {
      Sportcommunity.changePage();
      var data = await UserService.get(state.params.id);
      console.log(data);
      if(data) {
        return <UserPage data={data}/>;
      } else {
        return
      }
    });

    on('/sportcommunity/user/:id/edit', async (state) => {
      Sportcommunity.changePage();
      var data = await UserService.get(state.params.id);
      console.log(data);
      if (data) {
        return <EditUserPage data={data}/>;
      } else {
        return
      }

    });

    on('/sportcommunity/*', async (state) => {
      const query = `/api/content?path=${state.path}`;
      const response = await fetch(query);
      const data = await response.json();
      if (data && data.content) {
        Sportcommunity.changePage();
        return <Content {...data} />;
      } else { return false; }
    });


  }

  getChildContext() {
    const context = this.props.context;
    return {
      insertCss: context.insertCss || emptyFunction,
      onSetTitle: context.onSetTitle || emptyFunction,
      onSetImage: context.onSetImage || emptyFunction,
      onSetMeta: context.onSetMeta || emptyFunction,
      onPageNotFound: context.onPageNotFound || emptyFunction,
      onSetUser: context.onSetUser || emptyFunction

    };
  }


  componentWillMount() {
    const { insertCss } = this.props.context;
    insertCss(theme);
  }

  async componentDidMount() {
    window.addEventListener('changePage', this.onChangePage.bind(this));

  }

  componentWillUnmount() {
    window.removeEventListener('changePage', this.onChangePage.bind(this));
  }

  checkPath(to) {
    if (typeof window != 'undefined') {
      return window.location.pathname == to || (to != '/' && window.location.pathname.search(to) >= 0);
    }
    return false;
  }

  onChangePage() {
    setTimeout(() => {
      this.setState({ menu: false, basket: false, popup: false });
    }, 100);
  }

  userLogin(cb, errorCb) {
  (async () => {
    var respond = await fetch('/api/auth', { credentials: 'include' });
    var model = await respond.json();
    return model;
  })().then((user) => {
    var tmpUser = user.error ? false : user;
    this.setState({ user: tmpUser });
    console.log(tmpUser)
    if (tmpUser) {
      cb();
    } else {
      if (errorCb) {
        errorCb();
      }
    }
  });
}

setOptions(options, cb) {
  this.setState({ options: options }, () => {
    if (cb) {
      cb();
    }
  });
}

  render() {
    var children = React.cloneElement(this.props.children, {
      user: this.state.user && !this.state.user.error ? this.state.user : false,
      userLogin: this.userLogin.bind(this),
      options: this.state.options,
      setOptions: this.setOptions.bind(this)
    })
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <div id="wrapper" className="root">
          <Header user={this.state.user} />
          {children}
          <Footer/>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default Sportcommunity;
