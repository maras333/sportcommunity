import React, {Component, PropTypes} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import testAwatarPhoto from "../../../../images/testowe_photo.jpg";
import Link from '../../../common/components/Link/Link.js';

import UserServices from '../../../../services/UserServices/UserServices.js';

class CardExampleWithAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      likedImgs: {},
      ids: [],
    }
  }

  componentDidMount() {
    (async() => {
      var user = await UserServices.get(this.props.image.userId);
      this.setState({
        user: user,
      });
      console.log(this.state.user);

      var likedPhotos = await UserServices.getLikedPhotos();
      this.setState({
        likedImgs: likedPhotos,
      });
      console.log(this.state.likedImgs);

      if(this.state.likedImgs.lImages) {
        var tempIds = this.state.ids;
        for(let el of this.state.likedImgs.lImages) {
          console.log(el);
          tempIds.push(el['likedImages']);
          console.log(tempIds);
          this.state.ids = tempIds;
          this.setState({ids: this.state.ids});
          console.log(this.state.ids);
        }
      }
    })();

  }

  handleChange(event) {
    console.log('action button here like!');
    (async() => {
      var liked = await UserServices.setLikedPhoto(this.props.image.id);
      console.log(this.state.likedImgs);
      var likeObj = this.state.likedImgs;
      console.log(likeObj.lImages);
      likeObj.lImages.push(liked['0']['0']);
      console.log(likeObj.lImages);
      this.state.likedImgs = likeObj;

      this.setState({
        likedImgs: this.state.likedImgs,
      });
      console.log(this.state.likedImgs);
      console.log(this.state.likedImgs.lImages);
      if(this.state.likedImgs.lImages) {
        var tempIds = this.state.ids;
        for(let el of this.state.likedImgs.lImages) {
          console.log(el);
          tempIds.push(el['likedImages']);
          console.log(tempIds);
          this.state.ids = tempIds;
          this.setState({ids: this.state.ids});
          console.log(this.state.ids);
        }
      }

    })();
  }


  handleChangeRemove(event) {
    console.log('action button here unlike!');
    (async() => {
      var unliked = await UserServices.removeLikedPhoto(this.props.image.id);
      console.log(unliked);
      console.log(unliked['imageId']);
      var likeObj = this.state.likedImgs;
      for(let el of likeObj.lImages) {
        if(el['likedImages'] == unliked['imageId']) {
          var idx = likeObj.lImages.indexOf(el);
          likeObj.lImages.splice(idx, 1);
        }
      }
      // likeObj.lImages.push(liked['0']['0']);
      console.log(likeObj.lImages);
      this.state.likedImgs = likeObj;

      this.setState({
        likedImgs: this.state.likedImgs,
      });
      console.log(this.state.likedImgs);
      console.log(this.state.likedImgs.lImages);
      if(this.state.likedImgs.lImages) {
        var tempIds = this.state.ids;
        for(let el of this.state.likedImgs.lImages) {
          console.log(el);
          tempIds.push(el['likedImages']);
          console.log(tempIds);
          this.state.ids = tempIds;
          this.setState({ids: this.state.ids});
          console.log(this.state.ids);
        }
      }

    })();
  }

  render() {

    return (
      <Card style={{width: '60%', margin: '50px auto'}}>
        <Link to={"/sportcommunity/user/"+this.props.image.userId}>
          <CardHeader
            title={this.state.user.user.firstName+"  "+this.state.user.user.lastName}
            avatar={testAwatarPhoto}
          >
          </CardHeader>
        </Link>
        <CardMedia
          overlay={<CardTitle title={this.props.image.name} />}

        >
          <img src={this.props.image.url} />
        </CardMedia>
        <CardText>
          {this.props.image.caption}
        </CardText>
        <CardActions>
          {this.state.ids.indexOf(this.props.image.id)==-1 ?
            <FlatButton label="Like" onTouchTap={this.handleChange.bind(this)}/>  :
            <FlatButton label="Unlike" onTouchTap={this.handleChangeRemove.bind(this)}/>
          }

        </CardActions>
      </Card>
    );
  }
}

export default CardExampleWithAvatar;
