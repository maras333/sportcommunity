import React, {Component} from 'react';
import TextField from 'material-ui/TextField';

class TextFieldExampleSimple extends Component {

  constructor(props) {
    super(props);
  }

  handleChange(event) {
    this.props.handleChange(event);
  };

  render() {
    return (
      <div>
        <TextField
          fullWidth={true}
          floatingLabelText="Firstname"
          value={this.props.user.firstName}
          onChange={this.handleChange.bind(this)}
          name="firstName"
        /><br />
        <TextField
          fullWidth={true}
          floatingLabelText="Lastname"
          value={this.props.user.lastName}
          onChange={this.handleChange.bind(this)}
          name="lastName"
        /><br />
        <TextField
          fullWidth={true}
          floatingLabelText="Description"
          value={this.props.user.description}
          onChange={this.handleChange.bind(this)}
          name="description"
        />
      </div>
    );
  }
}

export default TextFieldExampleSimple;
