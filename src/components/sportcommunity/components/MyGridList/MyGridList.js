import React, {Component} from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import OnePhotoModal from '../OnePhotoCard/OnePhotoCard.js';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '80%',
    overflowY: 'auto',
    marginBottom: 24,
  },
};

class GridListExampleSimple extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      currentImage: {},
    }
  }

  handleOpen (image) {
    this.setState({open: true, currentImage: image});
  }

  handleClose(image) {
    this.setState({open: false});
  }

  handleRemove(image) {
    console.log('removed');
    console.log(image.id);
    this.props.handleRemove(image);
    this.setState({
      open: false
    });

  }



  render () {

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose.bind(this)}
      />
    ];

    return (
      <div style={styles.root}>
        <GridList
          cellHeight={250}
          style={styles.gridList}
          cols={3}
          padding={20}
        >
          {this.props.images.map((image) => (
              <GridTile
                onTouchTap={this.handleOpen.bind(this, image)}
                key={image.id}
                title={image.name}
                actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
              >
                <img style={{width: '100%'}} src={image.url} />
              </GridTile>
          ))}
        </GridList>
        <div>
          <Dialog
            actions={actions}
            modal={false}
            open={this.state.open}
            onRequestClose={this.handleClose}
            autoScrollBodyContent={true}
          >
            <OnePhotoModal user={this.props.user} image={this.state.currentImage} handleRemove={this.handleRemove.bind(this)}>

            </OnePhotoModal>
          </Dialog>
        </div>

      </div>
    );
  }
}

export default GridListExampleSimple;
