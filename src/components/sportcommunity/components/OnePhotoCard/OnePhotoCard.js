import React, {Component, PropTypes} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import testPhoto from "../../../../images/testowe_photo.jpg";
import testAwatarPhoto from "../../../../images/testowe_photo.jpg";
class CardExampleWithAvatar extends Component {
  constructor(props) {
    super(props);
  }

  handleRemove(image) {
    this.props.handleRemove(image);

  }

  render() {

    return (
      <Card style={{width: '100%', margin: '50px auto'}}>
        <CardHeader
          title={this.props.user.user.firstName+" "+this.props.user.user.lastName}
          avatar={testAwatarPhoto}
        />
        <CardMedia
          overlay={<CardTitle title={this.props.image.name} />}

        >
          <img src={this.props.image.url} />
        </CardMedia>
        <CardText>
          {this.props.image.caption}
        </CardText>
        <CardActions>
          {this.props.user.currentUser ? (this.props.user.currentUser.id == this.props.user.user.id ?
            <FlatButton label="Usuń" onTouchTap={this.handleRemove.bind(this, this.props.image)} /> : "")      
            :
            ""
          }
        </CardActions>
      </Card>
    );
  }
}

export default CardExampleWithAvatar;
