/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import {Collapse} from 'react-bootstrap';

class Chart extends Component {

  constructor(props) {
    super(props);

    this.state = {
      active: false
    }
  }

  handleActive() {
    this.setState({
      active: !this.state.active
    })
  }

  render() {

    return (
      <div className="chart-reservation">
       <div className="chart-container clerfix">
        <div className="row clear-bs">
          <div className="col-md-6 clear-bs">
            <div className="chart-logo"></div>
          </div>
          <div className="col-md-6 clear-bs">
            <p className="chart-logo-text">
              Ignimint otaquis non excea volore plam expernam autem voluptatur, audis dio quo inciaturepel ipsum natiatur aped quunt et ex essit quisquae pa delis reic to te nes qui aut volupta vidus sa vel ipsapic iendelecum et aut ex et ea dolorios volupis veliquo magnihitaepe nis nia quam, quatiaspid quae con comnit rem fugiaepedio. Abor aspero od mos solorepra quam quas dolestem ea conem volestius et untiis sinimi, nis ratem re sit fugiatis.
            </p>
          </div>
        <div className="row clear-bs">
          <div className="col-md-12 clear-bs">
            <div className="chart-image"></div>
          </div>
        </div>
        </div>
       </div>
      </div> // end of search class
    )
  }

}

export default Chart;
