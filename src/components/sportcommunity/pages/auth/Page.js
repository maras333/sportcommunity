/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import Link from '../../../common/components/Link';
import LoginForm from '../../../common/components/LoginForm';
import RegisterForm from '../../../common/components/RegisterForm';
import Location from '../../../../core/Location';


const title = 'Signup';

class Page extends Component {



  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.context.onSetTitle(title);
    this.context.onPageNotFound();
  }

  handleLogin() {
    this.props.userLogin(() => {
      Location.push('/sportcommunity/');
    });
  }

  render() {
    console.log(this.props);
    return (
      <div className="auth clearfix">
        <div className="auth-container">
          <div className="row clear-bs">
            <div className="col-md-4 hidden-sm clear-bs">
            </div>
            <div className="col-md-8 col-sm-12 clear-bs">
              <LoginForm successLogin={this.handleLogin.bind(this) }/>
            </div>
          </div>

          <div className="row clear-bs auth-space">
            <div className="col-md-4 hidden-sm clear-bs">
            </div>
            <div className="col-md-8 col-sm-12 clear-bs">
              <RegisterForm successLogin={this.handleLogin.bind(this) }/>
            </div>
          </div>

        </div>
      </div>
    );
  }

}

export default Page;
