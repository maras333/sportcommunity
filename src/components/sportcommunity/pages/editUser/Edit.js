
import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Location from '../../../../core/Location';
import UserService from '../../../../services/UserServices/UserServices.js';
import EditForm from '../../components/MyEditUserForm/MyEditUserForm.js';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar';



const title = 'User Edit';

class UserEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: 0,
      error: 0,
      message: 0,
      user: props.data,
      activeApi: false,

    };

  }

  static propTypes = {
    data: PropTypes.object.isRequired,
  };

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.context.onSetTitle(title);
  }

  handleChange(event) {
    var obj = this.state.user;
    obj['user'][event.target.name] = event.target.value;
    this.setState({ user: obj });
  }

  onSubmit(event) {
    console.log('on submit works');
    event.preventDefault();

    if (this.state.activeApi) {
      return 0;
    }

    this.setState({ activeApi: true });

    (async () => {
      try {
        console.log(this.state.user);
        var message = await UserService.edit(this.state.user.user);
        console.log(message);
        this.setState({ activeApi: false });
        if (message.error) {
          this.setState({ success: 0, error: 1, message: message.error.map((error) => {error.message}) });
        } else {
          this.setState({ success: 1, error: 0 });
          Location.push(`/sportcommunity/user/${this.state.user.user.id}`);
        }
      } catch (e) {
        this.setState({ error: 1, success: 0 });
      }
    })();
  }


  render() {
    const style = {margin: 5};
    this.context.onSetTitle(title);
    console.log(this.state.user.user);

    return (
      <div style={{width: '70%', margin: '3em auto'}}>
        <div className="row" >
          <div className="col-xs-12" >
            <div className="row">
              <div className="col-sm-offset-2 col-xs-2">
                <Avatar
                  src="https://upload.wikimedia.org/wikipedia/commons/3/36/Jaguar_XJ_3.0_D-S_Supersport_(X351)_%E2%80%93_Frontansicht,_30._Juni_2013,_M%C3%BCnster.jpg"
                  size={100}
                  style={style}
                />
              </div>
              <div className="col-sm-offset-1 col-xs-6" style={{paddingTop: '30px', paddingBotton: '30px', fontSize: '25px'}}>
                {this.state.user.user.username}
              </div>
            </div>
            <div className="row" >
              <div className="col-sm-offset-2 col-xs-8">
                <form onSubmit={this.onSubmit.bind(this) }>
                  <EditForm user={this.state.user.user} handleChange={this.handleChange.bind(this)} />
                  <RaisedButton type="submit" label="Zapisz" fullWidth={true} primary={true} />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserEdit;
