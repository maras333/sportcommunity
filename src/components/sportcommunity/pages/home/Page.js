/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import ImageServices from '../../../../services/ImageServices/imageServices.js';
import PhotoCard from '../../components/MyCard/MyCard.js';

const title = 'SportCommunity';

class Page extends Component {

  constructor(props) {
    super(props);
    this.state = {
      images:[]
    }
  }

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired,
  };

  componentDidMount() {
    (async() => {
      var data = await ImageServices.list();
      this.setState({
        images: data.images,
      });
      console.log(this.state.images);
    })();

  }

  componentWillMount() {
    this.context.onSetTitle(title);
    this.context.onPageNotFound();
  }

  render() {
    console.log(this.state.images);
    var imgCards=[];
    this.state.images.forEach((img) => {
      if(img.userId != null) {
        imgCards.push(<PhotoCard image={img}></PhotoCard>);
      }

    });

    return (
      <div>
        {imgCards}
      </div>
    );
  }
}

export default Page;
