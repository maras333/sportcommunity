/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import Link from '../../../common/components/Link';


const title = 'Info';

class Page extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
    onPageNotFound: PropTypes.func.isRequired,
  };

  componentWillMount() {
    // this.context.onSetTitle(title);
    // this.context.onPageNotFound();
  }

  render() {
    return (
     <div className="info clearfix">
        <div className="info-container">
          <div className="row clear-bs">
            <div className="col-md-4 clear-bs">
              <h2 className="clear-bs">O aplikacji</h2>
            </div>
            <div className="col-md-8 clear-bs">
              <div className="terms">
                <p className="info-description clear-bs">
                  Lis, maioruratis non Itanuludam tamqua nonlosu pient, vit publinc ullarbis atum tum sunum omnos coniuro elartur. Cas ad ceret; hostem mover que convenatum ut inti, conderfin hoc iusperfestod aucio, cus verra re nos huisque acernum atquam rectore issena.
                </p>
                <div >
                  <Link to="https://facebook.github.io/react/">
                      React JS
                  </Link>
                </div>
                <div >
                  <Link to="http://www.material-ui.com">Material UI</Link>
                </div>
                <div >
                  <Link target="_blank" to="https://expressjs.com">ExpressJS</Link>
                </div>
              </div>
            </div>
          </div>

          <div className="row clear-bs">
            <div className="col-md-4 clear-bs">
              <h2 className="clear-bs">Wykonanie</h2>
            </div>
            <div className="col-md-8 clear-bs">
              <div className="terms">
                <p className="info-description clear-bs">
                  Lis, maioruratis non Itanuludam tamqua nonlosu pient, vit publinc ullarbis atum tum sunum omnos coniuro elartur. Cas ad ceret; hostem mover que convenatum ut inti, conderfin hoc iusperfestod aucio, cus verra re nos huisque acernum atquam rectore issena.
                </p>
                <div>
                  <Link to="/sportcommunity/">Marek Czyż</Link>
                </div>
              </div>
            </div>
          </div>

          <div className="row clear-bs">
            <div className="col-md-4 clear-bs">
              <h2 className="clear-bs">Support</h2>
            </div>
            <div className="col-md-8 clear-bs">
              <div className="terms last">
                <p className="info-description clear-bs">
                  Lis, maioruratis non Itanuludam tamqua nonlosu pient, vit publinc ullarbis atum tum sunum omnos coniuro elartur. Cas ad ceret; hostem mover que convenatum ut inti, conderfin hoc iusperfestod aucio, cus verra re nos huisque acernum atquam rectore issena.
                </p>
                <div>
                  <Link to="/sportcommunity/">Support</Link>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }

}

export default Page;
