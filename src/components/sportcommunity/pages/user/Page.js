/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import Link from '../../../common/components/Link';
import Avatar from 'material-ui/Avatar';
import GridList from '../../components/MyGridList/MyGridList.js';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import EventServices from '../../../../services/EventServices/EventServices.js';
import UserServices from '../../../../services/UserServices/UserServices.js';

const title = 'User view';

class User extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      user: props.data,
    }



  }

  componentWillMount() {
    // this.context.onSetTitle(title);

  }

  componentDidMount() {

    this.state = {
      user: this.props.data,
    };
    (async() => {
      var likedPhotos = await UserServices.getLikedPhotos();
      this.setState({
        likedImgs: likedPhotos,
      });
      console.log(this.state.likedImgs);
    })();



  }

  handleRemove(img) {
    console.log('removid');
    console.log(img);
    (async() => {
      var image = await UserServices.removeImage(img.id);
      var res = image.json();
      console.log(res);
    })();
  }

  render() {

    console.log(this.state.user);
      const style = {margin: 5};
    return (
      <div style={{margin: '50px auto', width: '80%'}}>
        <div className="row">
          <div className="col-xs-4">
            <Avatar
              src="https://commons.wikimedia.org/wiki/File:Army_Photography_Contest_-_2007_-_FMWRC_-_Arts_and_Crafts_-_Eye_of_the_Holder_(4930275692).jpg"
              size={180}
              style={style}
            />
          </div>
          <Table selectable={false}>
            <TableBody stripedRows={true} displayRowCheckbox={false} >
              <TableRow  >
                <TableRowColumn> {this.state.user.user.username} </TableRowColumn>
                <TableRowColumn ></TableRowColumn>
                <TableRowColumn>
                  <Link to={`/sportcommunity/user/${this.state.user.user.id}/edit`}>
                    <FlatButton label="Edit..." primary={true}></FlatButton>
                  </Link>
                </TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>Posts: {this.state.user.user.images.length}</TableRowColumn>
                <TableRowColumn>Liked photos: {this.state.likedImgs.count}</TableRowColumn>
                <TableRowColumn>Following photos:</TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
          <Table selectable={false}>
            <TableBody stripedRows={true} displayRowCheckbox={false} >
              <TableRow>
                <TableRowColumn>{this.state.user.user.firstName + "  " + this.state.user.user.lastName + ":  " + this.state.user.user.description}</TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>

        </div>
        <div style={{marginTop: '40px'}}>
          <GridList user={this.state.user} images={this.state.user.user.images} handleRemove={this.handleRemove.bind(this)}>

          </GridList>

        </div>
      </div>
    );
  }

}

export default User;
