/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import Link from '../../../common/components/Link';
import Avatar from 'material-ui/Avatar';
import GridList from '../../components/MyGridList/MyGridList.js';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import EventServices from '../../../../services/EventServices/EventServices.js';
import Location from '../../../../core/Location';


class UserList extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      users: props.data.users
    }

  }

  componentWillMount() {
    // this.context.onSetTitle(title);

  }

  componentDidMount() {

  }

  handleChange(user) {
    Location.push(`/sportcommunity/user/${user.id}`);
  }

  render() {

    console.log(this.state.users);
    var rows = [];
      this.state.users.map((user) => {
        rows.push(
          <TableRow onTouchTap={this.handleChange.bind(this, user)}>
            <TableHeaderColumn>{user.username}</TableHeaderColumn>
            <TableHeaderColumn>{user.firstName}</TableHeaderColumn>
            <TableHeaderColumn>{user.lastName}</TableHeaderColumn>
          </TableRow>
        );
      });
      const style = {margin: 5};
    return (
      <div style={{margin: '50px auto', width: '80%'}}>
        <div className="row">
          <Table selectable={false}>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow >
                <TableHeaderColumn><span style={{fontWeight: 'bold'}}>Username</span></TableHeaderColumn>
                <TableHeaderColumn><span style={{fontWeight: 'bold'}}>First Name</span></TableHeaderColumn>
                <TableHeaderColumn><span style={{fontWeight: 'bold'}}>Last Name</span></TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody stripedRows={true} displayRowCheckbox={false} >
              {rows}
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }

}

export default UserList;
