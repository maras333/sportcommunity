/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable max-len */
/* jscs:disable maximumLineLength */

export const port = process.env.PORT || 3000;
export const host = process.env.WEBSITE_HOSTNAME || `localhost:${port}`;

export const database = {
  server: process.env.SERVER_DB || "localhost",
  login: process.env.LOGIN_DB || "root",
  password: process.env.PASSWORD_DB || "",
  db: process.env.NAME_DB || "sportcommunity"
};

export const analytics = {

  // https://analytics.google.com/
  google: { trackingId: process.env.GOOGLE_TRACKING_ID || 'UA-XXXXX-X' },

};

export const auth = {

  jwt: { secret: process.env.JWT_SECRET || 'React Starter Kit' },

  cookie: process.env.COOKIE || "blableasdad",
  // https://developers.facebook.com/
  facebook: {
    id: process.env.FACEBOOK_APP_ID || '1045547958820487',
    secret: process.env.FACEBOOK_APP_SECRET || '040e9c0be980f966bf67c79a20cef24e',
  },

  // https://cloud.google.com/console/project
  google: {
    id: process.env.GOOGLE_CLIENT_ID || '746742134279-iutj4e7nn1gstmljib07oe0ddvva8g0e.apps.googleusercontent.com',
    secret: process.env.GOOGLE_CLIENT_SECRET || 'LyHlepkK0-BAczdJBNudoTFt',
  },

};

export const filePath = '/images/';

export const smtpConfig = {
  host: "mailtrap.io",
  port: 2525,
  auth: {
    user: "28338f40acd2a328f",
    pass: "3c816596e0a4b3"
  }

};

export const contactConfig = {
  subject: "evermoto",
  to: "hello@evermoto"
};

export const emailConfig = {
  name: "evermoto",
  from: "hello@evermoto"
};
