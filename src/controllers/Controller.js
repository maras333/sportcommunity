/**
 * Created by jarek_000 on 29.02.2016.
 */

class Controller {
  constructor(){

  }

  register(router){}

  stdRegister(router,name){
    router
      .get(`/${name}`, Controller.getList)
      .put(`/${name}/:id`, Controller.put)
      .get(`/${name}/:id`, Controller.get)
      .delete(`/${name}/:id`, Controller.remove)
      .post(`/${name}`, Controller.post);

  }

  async getList(req, res, next){
    var page = req.query.page || 1;
    page = parseInt(page);

    var perPage = req.query.perPage || 10;
    var offset = req.query.offset || ((page-1)*perPage);

    offset = parseInt(offset);
    perPage = parseInt(perPage);

    var results = await this.model.findAndCountAll({
      offset,
      limit: perPage
    });
    var Models = results.rows;

    Models.forEach(u=>{
      u.password = ''
    });

    res.json(
      {
        meals: Models,
        count: results.count,
        pages: Math.ceil(results.count/perPage)
      }
    );

  }

  async get(req, res, next){
    var id = req.params.id;
    console.log(id);
    try {
      var model = await this.model.findOne({where:{id:id}, include: [{all:true}]});
    }catch (e){
      console.log(e);
      res.status(500).json(e);
      return;
    }

    res.json(model);

  }

  async put(req, res, next){
    var id = req.params.id;
    var newModel = req.body;
    var model = await this.model.findById(id);
    var attrs = this.model.getSafeAttr();

    Object.assign(model, newModel);

    try {
      model.save();
      model.save();

    }catch (e){
      console.log(e);
      res.status(500).json({error:e.errors});
      return 0;
    }

    res.json(model);

  }

  async post(req, res, next){
    var id = req.params.id;
    var newMeal = req.body;
    var meal = {};

    try {
      meal = await this.model.create(newMeal);
      await Controller.addNewProducts(newMeal, meal);
      await Controller.addTags(newMeal, meal);
      meal.save();

    }catch (e){
      console.log(e);
      res.status(500).json({error:e.errors});
      return 0;
    }

    res.json(meal);

  }

  async remove(req, res){
    var id = req.params.id;
    try {
      var model = await this.model.findById(id);
      await model.destroy();
      res.send({success:"model removed"})

    }catch (e){
      res.status(500).send({error:e.errors})
    }
  }

};
export default Controller;
