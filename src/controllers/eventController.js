
import Image from '../models/imageModel';
import Event from '../models/eventModel';
import multer from 'multer';
import path from 'path';
import crypto from 'crypto';
import mime from 'mime';
import {filePath} from '../config';
import {isAdmin, haveAccess, isLogin} from '../core/middleware';
import request from 'request';


var eventController = {
  register:(router)=>{
    router
      .get('/events', eventController.getList)
      .get('/event/:id', eventController.get)

  },



  getList: async (req, res, next)=> {
    var page = req.query.page || 1;
    page = parseInt(page);

    var perPage = req.query.perPage || 3;
    var offset = req.query.offset || ((page-1)*perPage);

    offset = parseInt(offset);
    perPage = parseInt(perPage);

    var results = await Event.findAndCountAll({
      offset,
      limit: perPage
    });
    var events = results.rows;

    res.json(
      {
        events: events,
        count: results.count,
        //pages: Image.ceil(results.count/perPage)
      }
    );

  },



  get: async (req, res, next)=> {
    var id = req.params.id;
    console.log(id);
    try {
      var event = await Event.findOne({where:{id:id}});
    }catch (e){
      console.log(e);
      res.status(500).json(e);
      return;
    }

    res.json(event);

  },

};
export default eventController;
