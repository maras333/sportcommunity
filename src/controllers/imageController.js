
import Image from '../models/imageModel';
import multer from 'multer';
import path from 'path';
import crypto from 'crypto';
import mime from 'mime';
import {filePath} from '../config';
import sizeOf from 'image-size';
import {isAdmin, haveAccess, isLogin} from '../core/middleware';

var imageController = {
  register:(router)=>{
    router
      .get('/images', imageController.getList)
      .get('/image/:id', imageController.get)
      .post('/images', imageController.uploading.single('file'), imageController.post)
      .put('/image/:id', imageController.put);
  },

  getList: async (req, res, next)=> {
    console.log(req.user);
    var page = req.query.page || 1;
    page = parseInt(page);

    var perPage = req.query.perPage || 3;
    var offset = req.query.offset || ((page-1)*perPage);

    offset = parseInt(offset);
    perPage = parseInt(perPage);

    var results = await Image.findAndCountAll({
      // offset,
      // limit: perPage
    });
    var images = results.rows;

    res.json(
      {
        images: images,
        // count: results.count,
        //pages: Image.ceil(results.count/perPage)
      }
    );

  },

  get: async (req, res, next)=> {
    var id = req.params.id;
    console.log(id);
    try {
      var image = await Image.findOne({where:{id:id}});
    }catch (e){
      console.log(e);
      res.status(500).json(e);
      return;
    }

    res.json(image);

  },

  uploading: multer({
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null,  __dirname + '/public'+filePath);
      },

      filename: function (req, file, cb) {
        var ext = path.extname(file.originalname);
        ext = ext.length>1 ? ext : "." + mime.extension(file.mimetype);
        crypto.pseudoRandomBytes(16, function (err, raw) {
          cb(null, (err ? undefined : raw.toString('hex') ) + ext);
        });
      }
    })
  }),


  post: async (req, res, next)=> {
    console.log(req.user);
    var dimensions = sizeOf(__dirname + '/public/'+filePath+req.file.filename);

    var newImage = {
      size: req.file.size,
      url: filePath+req.file.filename,
      width: dimensions.width,
      height: dimensions.height,
      userId: req.user.id,
    };
    var image = {};

    try {
      image = await Image.create(newImage);
      image.save();
      console.log(image);


    }catch (e){
      console.log(e);
      res.status(500).json({error:e});
      return 0;
    }

    res.json(image);

  },

  put: async (req, res, next)=> {
    try {
      console.log('image put working');
      var id = req.params.id;
      var newImage = req.body;
      console.log(req.user);
      var image = {};
      image = await Image.findById(id);
      image['caption'] = newImage['caption'];
      image['time'] = newImage['time'];
      image['name'] = newImage['name'];
      // image['userId'] = 1;
      image.save();
    } catch (e){
      console.log(e);
      res.status(500).json({error:e});
      return 0;
    }

    res.json(image);

  },


};
export default imageController;
