
import User from '../models/userModel';
import Image from '../models/imageModel.js';
import Like from '../models/likeModel.js';
import mail from '../core/mail';
import {
  config
} from '../config';
import {
  isAdmin,
  haveAccess,
  isLogin
} from '../core/middleware';
import crypto from 'crypto';
import passport from 'passport';

var userController = {
  register: (router) => {
    router
      .get('/users', userController.getList)
      .post('/passwordChange', userController.changePassword)
      .get('/verification/:id', userController.verification)
      .get('/user/logout', userController.logout)
      .put('/user/:id', isLogin(), userController.put)
      .put('/user', isLogin(), userController.putUser)
      .get('/user/:id', userController.get)
      .delete('/user/:id', isAdmin(), userController.delete)
      .get('/auth', userController.getAuth)
      .post('/user', isAdmin(), userController.post)
      .post('/register', userController.userRegister)
      .post('/users/likePhoto/:id', userController.setLikedPhoto)
      .delete('/users/likePhoto/:id', userController.removeLikedPhoto)
      .get('/users/likedPhotos', userController.getLikedPhotos)
      .delete('/user/images/:id', userController.removeImage);

  },

  getList: async (req, res, next) => {
    var page = req.query.page || 1;
    page = parseInt(page);
          console.log(req.user);

    var perPage = req.query.perPage || 10;
    var offset = req.query.offset || ((page - 1) * perPage);
    var search = {};

    for (var i of ['username', 'firstName', 'lastName', 'email']) {
      if (req.query[i])
        search[i] = {
          $like: `%${req.query[i]}%`
        };
    }

    offset = parseInt(offset);
    perPage = parseInt(perPage);



    var results = await User.findAndCountAll({
      where: search,
      offset,
      limit: perPage
    });
    var users = results.rows;

    users.forEach(u => {
      u.password = ''
    });

    res.json({
      users: users,
      count: results.count,
      pages: Math.ceil(results.count / perPage)
    });

  },

  get: async (req, res, next) => {
    var id = req.params.id;
    console.log(req.user);

    try {
      var currentUser;
      if (req.user) {
        currentUser = req.user;
      }
      var user = await User.findOne({
        where: {
          id: id
        },
        include: [{
          all: true,
          nested: false
        }]
      });
    } catch (e) {
      console.log(e);
      res.json({
        "error": true
      });
    }
    user.password = '';
    res.json({
      user: user,
      currentUser: currentUser,
    });

  },

  put: async (req, res, next) => {
    var id = req.params.id;
    var newUser = req.body;
    var user = await User.findOne({
      where: {
        id: id
      },
      include: [{
        all: true,
        nested: true
      }]
    });

    user.firstName = newUser.firstName ? newUser.firstName : user.firstName;
    user.lastName = newUser.lastName ? newUser.lastName : user.lastName;
    user.description = newUser.description ? newUser.description : user.description;

    try {
      if(req.user.id == id) {
        user.save();
        res.json(user);
      } else {
        res.status(401).json({
          error: "Unauthorized",
        });
      }

    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: "User not changed"
      });
    }



  },
  putUser: async (req, res, next) => {
    var id = req.params.id;
    var newUser = req.body;
    var user = req.user;
    try {
      user.username = newUser.email ? newUser.email : user.email;
      user.email = newUser.email ? newUser.email : user.email;
      user.firstName = newUser.firstName ? newUser.firstName : user.firstName;
      user.lastName = newUser.lastName ? newUser.lastName : user.lastName;
      user.gender = newUser.gender ? newUser.gender : user.gender;
      user.phone = newUser.phone;
      user.sms = newUser.sms ? newUser.sms : user.sms;

      if (newUser.password) {
        user.password = crypto.createHash('md5').update(newUser.password).digest("hex");
      }
      user.save();
    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: "User not changed"
      });
      return 0;
    }

    res.json(user);
    return 0;
  },
  post: async (req, res, next) => {
    var id = req.params.id;
    var newUser = req.body;
    console.log(newUser);

    if (newUser.password) {
      newUser.password = User.setPassword(newUser.password);
    }
    var user = {};

    try {
      user = await User.create(newUser);

    } catch (e) {
      console.log(e)
      res.status(500).json({
        error: e.errors
      });
      return 0;
    }
    res.json(user);

  },

  userRegister: async (req, res, next) => {
    var id = req.params.id;
    var newUser = req.body;
    console.log(newUser);

    if (newUser.password) {
      newUser.password = User.setPassword(newUser.password);

    }

    newUser.credits = 0;

    var user = {};

    try {
      newUser.verToken = User.setPassword(Date.now().toString());
      newUser.username = newUser.email;
      user = await User.create(newUser);
      mail.welcomeEmail(newUser.email, newUser);


    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: e.errors ? e.errors : e
      });
      return 0;
    }
    req.login(user, function (err) {
      if (err) {
        return res.json({
          error: err
        });
      }
      res.json(user);

    });


  },

  verification: async (req, res, next) => {
    var id = req.params.id;
    var user = await User.find({
      where: {
        verToken: id
      }
    });

    if (user) {
      user.active = true;
      user.save();
      res.json(user)
    } else {
      res.json({
        "error": "not allowed"
      });
    }
  },

  generatePassword() {
    var length = 8,
      charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  },

  changePassword: async (req, res, next) => {
    var email = req.body.email;
    try {
      var user = await User.find({
        where: {
          email
        }
      });
      var newPassword = userController.generatePassword();

      if (user) {
        user.password = User.setPassword(newPassword);
        user.save();
        mail.newPassword(user.email, {
          firstName: user.firstName,
          newPassword
        });
        res.json({
          "success": "new password you got on email"
        });
      } else {
        res.json({
          "error": "not allowed"
        });
      }
    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: e.errors ? e.errors : e
      });
      return 0;
    }
  },

   getAuth: async (req, res, next) => {
           console.log(req.user);
    if (req.user) {

      req.user.password = '';
      var user = await User.findOne({
        where: {
          id: req.user.id
        },
        include: [{
          all: true,
          nested: false
        }]
      });
      user.password = '';
      res.json(user);
    } else {
      res.json({
        error: "not allowed"
      })
    }
  },

  getAuth2: (req, res, next) => {

  },
  logout: (req, res) => {
    req.logout();
    res.redirect('/sportcommunity/login');
  },
  delete: async (req, res) => {
    var id = req.params.id;
    console.log(id);
    try {
      var user = await User.findById(id);
      user.destroy();
      res.send({
        success: "user removed"
      })

    } catch (e) {
      console.log(e);
      res.status(500).send({
        error: "user doesn't remove"
      })
    }
  },

  setLikedPhoto: async (req, res, next) => {
    try {
      var imgId = req.params.id;
      console.log(imgId);
      var userId = req.user.id;
      console.log(userId);
      var image = await Image.findById(imgId);
      console.log(image.dataValues);
      var assUserImg = await image.addUser(userId);
      console.log(assUserImg);

    } catch (e) {
      res.status(500).json({
        error: e.errors
      });
      return 0;
    }
    res.json(assUserImg);
  },

  getLikedPhotos: async (req, res, next) => {
    try {
      if(req.user.id) {
        var likedImages = await Like.findAndCountAll({
          where: {
            likingUsers: req.user.id,
          }
        });
        console.log(likedImages);
        res.json({
          lImages: likedImages.rows,
          count: likedImages.count,
        });
      } else {

      }
    } catch (e) {
      res.status(500).json({
        error: e.errors
      });
      return 0;
    }
  },

  removeLikedPhoto: async (req, res, next) => {
    try {
      var imgId = req.params.id;
      console.log(imgId);
      var userId = req.user.id;
      console.log(userId);
      var image = await Image.findById(imgId);
      console.log(image.dataValues);
      var assUserImg = await image.removeUser(userId);
      console.log(assUserImg);

      res.json({
        imageId: imgId,
      });

    } catch (e) {
      res.status(500).json({
        error: e.errors
      });
      return 0;
    }

  },




  removeImage: async (req, res, next) => {
    try {
      var user = await User.findById(req.user.id);
      // console.log(img);
      // console.log(req.user.id);
      console.log(user);
      await user.removeImage(req.params.id);
    } catch (e) {
      res.status(500).json({
        error: e.errors
      });
      return 0;
    }
  },


};
export default userController;
