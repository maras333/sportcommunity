var acl = require('acl');

var getUserRoles = function (req) {
  var roles = ['guest'];

  if (req.user) {
    roles.push('user');

    if (req.user.isAdmin) {
      roles.push('admin');
    }
  }

  return roles;
};

var defaultAuthorization = function (accessData) {
  let aclInstance = new acl(new acl.memoryBackend());
  aclInstance.allow(accessData);

  return function (req, res, next) {
    aclInstance.areAnyRolesAllowed(getUserRoles(req), req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
      if (err) {
        return res.status(500).send('Unexpected authorization error');
      } else {
        if (isAllowed) {
          return next();
        } else {
          return res.status(403).json({
            message: 'User is not authorized'
          });
        }
      }
    });
  }
};

export { defaultAuthorization, getUserRoles }
