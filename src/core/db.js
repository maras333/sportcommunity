/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import Promise from 'bluebird';
import { database } from '../config';
import Sequelize from 'sequelize';


const db = new Sequelize(database.db, database.login, database.password, {
  host: database.server,
  port: database.port,
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

// db.sync({force:true});

export default db;
