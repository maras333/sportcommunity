import nodemailer from 'nodemailer';
import { smtpConfig, emailConfig, contactConfig} from './../config';
import striptags from 'striptags';
import fs from "fs";
import _ from "lodash";

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport(smtpConfig);
var mail = {
  welcomeEmail: (email, tags) => {
    mail.prepEmailAndSend("welcome", email, "Rejestracja w Justchopped.pl", tags);
  },
  verificationEmail: (email, tags) => {
    // mail.prepEmailAndSend("verification", email, "Cześć", tags);
  },
  newOrderPaid: (email, order) => {
    mail.prepEmailAndSend("new-order-paid", email, "Potwierdzenie złożenia zamówienia na JustChopped.pl", order);
  },
  newOrder: (email, order) => {
    mail.prepEmailAndSend("new-order", email, "Potwierdzenie złożenia zamówienia na JustChopped.pl", order);
  },
  newPassword: (email, tags) => {
    mail.prepEmailAndSend("change-password", email, "Nowe hasło użytkownika", tags);
  },
  contactEmail: (name, from, text, cb) => {
    var mailOptions = {
      from: `"${name}" <${from}>`, // sender address
      to: contactConfig.to, // list of receivers
      subject: `${contactConfig.subject}  <${from}>`, // Subject line
      text: text // plaintext body
    };
    mail.mailSend(mailOptions, cb);
  },

  prepEmailAndSend: (file, email, title, tags = false) => {
    fs.readFile(__dirname + `/content/email/${file}.html`, 'utf8', function (err, html) {
      if (tags) {
        var compiled = _.template(html);
        html = compiled(tags);
      }
      var mailOptions = {
        from: `"${emailConfig.name}" <${emailConfig.from}>`, // sender address
        to: email, // list of receivers
        subject: title, // Subject line
        text: striptags(html), // plaintext body
        html: html // html body
      };
      mail.mailSend(mailOptions);
    });
  },

  mailSend: (mailOptions, cb = null) => {
    transporter.sendMail(mailOptions, function (error, info) {
      try {
        cb && cb(error, info.response);
      } catch (e) {
        return error;
      }
      if (error) {
        return error;
      }
      console.log('Message sent: ' + info.response);
    });
  }
};

export default mail;
// setup e-mail data with unicode symbols
