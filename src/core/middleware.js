export function isAdmin() {
  return [
    function (req, res, next) {
      try {
        if (req.user && req.user.isAdmin)
          next();
        else
          res.status(401).json({ error: 'Unauthorized' });
      } catch (e) {
        res.status(500).json({ error: 'Unauthorized' });

        console.log(e);
      }
    }
  ];
};

export function haveAccess() {
  return [
    function (req, res, next) {
      try {
        if (req.user && req.user.isAdmin || req.user.id == req.params.id)
          next();
        else
          res.status(401).json({ error: 'Unauthorized' });
      } catch (e) {
        res.status(500).json({ error: 'Unauthorized' });

        console.log(e);
      }
    }
  ];
};

export function isLogin() {
  return [
    function (req, res, next) {
      try {
        if (req.user)
          next();
        else
          res.status(401).json({ error: 'Unauthorized' });
      } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Unauthorized' });

      }
    }
  ];
};
