/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/**
 * Passport.js reference implementation.
 * The database schema used in this sample is available at
 * https://github.com/membership/membership.db/tree/master/postgres
 */

import passport from 'passport';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as GoogleStrategy } from 'passport-google-oauth2';
import User from '../models/userModel';
import db from './db';
import { auth as config } from '../config';
import crypto from 'crypto';

function generatePassword() {
  var length = 8,
    charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
}

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  function (username, password, done) {
    return User.findOne({ where: {username: username, password: crypto.createHash('md5').update(password).digest("hex") } })
      .then(function (user) {
        if (user !== null) {
          console.log('[AUTH] Success with username: ' + user.username + ' and password (md5-hash): ' + user.password);
          return done(null, user);
        }
        else {
          console.log('[AUTH] Error with username: ' + username + ' and password:' + password);
          console.log('[AUTH] md5-hash of passed password: ' + crypto.createHash('md5').update(password).digest("hex"));
          return done(null, false);
        }
      })
  }
));

passport.use(new FacebookStrategy({
    clientID: config.facebook.id,
    clientSecret: config.facebook.secret,
    callbackURL: '/login/facebook/return'
  },
  function(accessToken, refreshToken, profile, done) {
    console.log(profile);
    User.findOne({where:{facebookId:profile.id}}).then(function(user) {
      if (user === null) {
        User.create({
          username: profile.displayName,
          password: crypto.createHash('md5').update(generatePassword()).digest("hex"),
          facebookId: profile.id
        }).then((user)=>{
            done(null,user);
        });
      }else {
        done(null, user);
      }
    });
  }
));

passport.use(new GoogleStrategy({
    callbackURL: 'http://localhost:3000/login/google/return',
    clientID: config.google.id,
    clientSecret: config.google.secret
  },
  function(request, accessToken, refreshToken, profile, done) {
    console.log(profile);
    User.findOne({where:{ googleId: profile.id }}).then(function(user) {
      if (user === null) {
        User.create({
          username: profile.displayName,
          password: crypto.createHash('md5').update(generatePassword()).digest("hex"),
          firstName: profile.name.givenName,
          lastNmae:  profile.name.familyName,
          email: profile.email,
          googleId: profile.id
        }).then((user)=>{
          done(null,user);
        });
      }else {
        done(null, user);
      }
    });
  }
));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  User.findById(id).then(function (user) {
    return cb(null, user);
  })
  .error((err)=> cb(err));
});

export default passport;
