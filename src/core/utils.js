
export function setGetParameter(url, params) {
  var ret = [];
  for (var paramName in params) {
    if (Array.isArray(params[paramName])) {
      for (var subParamName in params[paramName]) {
        let value = params[paramName][subParamName];

        ret.push(encodeURIComponent(paramName + "[]") + '=' + encodeURIComponent(value));
      }
    } else {
      if (params[paramName] instanceof Object === true) {
        for (var subParamName in params[paramName]) {
          if (params[paramName].hasOwnProperty(subParamName)) {

            let value = params[paramName][subParamName];

            ret.push(encodeURIComponent(`${paramName}[${subParamName}]`) + '=' + encodeURIComponent(value));
          }
        }
      } else {
        ret.push(encodeURIComponent(paramName) + '=' + encodeURIComponent(params[paramName]));
      }
    }
  }
  return url + '?' + ret.join('&');
}
export function getSlug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "ąęćółżźśń·/_,:;";
  var to = "aecolzzsn------";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}
