

import db from '../core/db';
import Sequelize from 'sequelize';
import crypto from 'crypto';
import Image from './imageModel.js';

var Event = db.define('event', {

  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING(255),
    allowNull: true,
  },

  time: {
    type: Sequelize.STRING(255),
    allowNull: true,
  },
  opinion: {
    type: Sequelize.STRING(255),
    allowNull: true,
  },

}, {
  freezeTableName: true, // Model tableName will be the same as the model name

});

Event.belongsTo(Image);
Image.hasOne(Event);

export default Event;
