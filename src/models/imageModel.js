
/**
 * Created by jarek_000 on 01.03.2016.
 */

import db from '../core/db';
import Sequelize from 'sequelize';
import crypto from 'crypto';
import User from './userModel.js';
import Like from './likeModel.js';

var Image = db.define('image', {

  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true,
  },

  caption: {
    type: Sequelize.STRING(255),
    allowNull: true,
  },

  url: {
    type: Sequelize.STRING(255),
    defaultValue: false,
    allowNull: false,
  },

  width:{
    type: Sequelize.INTEGER,
    allowNull: false
  },

  height:{
    type: Sequelize.INTEGER,
    allowNull: false
  },

  size:{
    type: Sequelize.STRING,
    allowNull: false
  },

  time:{
    type: Sequelize.STRING,
    allowNull: true
  },

  name:{
    type: Sequelize.STRING,
    allowNull: true
  },

}, {
  freezeTableName: true, // Model tableName will be the same as the model name

});

User.belongsToMany(Image, {
  through: {
    model: Like,
    unique: false,
  },
  foreignKey: 'likingUsers',
  // as: 'likedImages',
});

Image.belongsToMany(User, {
  through: {
    model: Like,
    unique: false,
  },
  foreignKey: 'likedImages',
  // as: 'likingUsers',
});

Image.belongsTo(User);
User.hasMany(Image, {as: 'images'})



export default Image;
