import DataType from 'sequelize';
import db from '../core/db';
import User from './userModel.js';

const Like = db.define('like', {
  id: {
    primaryKey: true,
    type: DataType.INTEGER,
    autoIncrement: true
  }
}, {
  classMethods: {

  },
  instanceMethods: {

  }
});

export default Like;
