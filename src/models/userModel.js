/**
 * Created by jarek_000 on 01.03.2016.
 */

import db from '../core/db';
import Sequelize from 'sequelize';
import crypto from 'crypto';

var User = db.define('user', {
  username: {
    type: Sequelize.STRING,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      isEmail: true
    }
  },
  firstName: {
    type: Sequelize.STRING,
    field: 'first_name' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  lastName: {
    type: Sequelize.STRING,
    field: 'last_name' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  description: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  active: {
    type: Sequelize.BOOLEAN

  },
  verToken: {
    type: Sequelize.STRING,
    field: "ver_token"
  },
  isAdmin: {
    type: Sequelize.BOOLEAN,
    field: "is_admin",
    defaultValue: false,
  },
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    classMethods: {
      setPassword: (password) => {
        return crypto.createHash('md5').update(password).digest("hex");
      },
      getSafeAttr: () => {
        return[
          "firstName",
          "lastName",
          "description",
        ];
      }
    },
    instanceMethods: {
      setPassword: (password) => {
        this.password = crypto.createHash('md5').update(password).digest("hex");
        return this.password;
      }
    }
  });




export default User;
