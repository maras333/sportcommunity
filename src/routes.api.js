import express from 'express';

var router = express.Router();

import userController from './controllers/userController';
import imageController from './controllers/imageController';
import eventController from './controllers/eventController';

userController.register(router);
imageController.register(router);
eventController.register(router);


router.all('*', (req,res)=>{
  res.send({error:"invalid"})
});

export default router;
