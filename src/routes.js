/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Router from 'react-routing/src/Router';

import Sportcommunity from './components/sportcommunity/Sportcommunity.js';
import Common from './components/common';

const router = new Router(on => {

Sportcommunity.registerRouter(on);

 on('/', async (state) => {
   Sportcommunity.changePage();
   return <Common.MainHome context={state.context} />;
 });
});

export default router;
