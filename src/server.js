/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import 'babel-polyfill';
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import expressJwt from 'express-jwt';
import jwt from 'jsonwebtoken';
import React from 'react';
import ReactDOM from 'react-dom/server';
import expressSession from 'express-session';
import flash from 'connect-flash';
import passport from './core/passport';
import Router from './routes';
import Html from './components/html';
import db from './core/db';
import assets from './assets';
import fetch from './core/fetch';
import { port, auth } from './config';
import router from './routes.api';


const server = global.server = express();

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
server.use(express.static(path.join(__dirname, 'public')));
server.use(cookieParser());
server.use(expressSession({
  secret : auth.cookie,
  cookie : {
    expires: false
  },
  resave: true,
  saveUninitialized: true
}));
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

//
// Login to DB
//
global.db = db;

//
// Authentication
// -----------------------------------------------------------------------------
server.use(expressJwt({
  secret: auth.jwt.secret,
  credentialsRequired: false,
  /* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
  getToken: req => req.cookies.id_token,
  /* jscs:enable requireCamelCaseOrUpperCaseIdentifiers */
}));


/** Passport init **/
server.use(passport.initialize());
server.use(passport.session());
server.use(flash());

server.get('/login/facebook',
  passport.authenticate('facebook', { scope: ['email'] })
);
server.get('/login/facebook/return',
  passport.authenticate('facebook', { successRedirect: '/successLogin', failureRedirect: '/login' })
);

server.post('/login/local', passport.authenticate('local', {
  failureFlash: true
}), (req, res) => {
  res.json({ success: true });
}, (req, res) => {
  res.json({ error: true });
}
);
server.get('/login/google',
  passport.authenticate('google', {
    scope:
    ['https://www.googleapis.com/auth/plus.login'
      , 'https://www.googleapis.com/auth/plus.profile.emails.read']
  })
);

server.get('/login/google/return',
  passport.authenticate('google', { successRedirect: '/successLogin', failureRedirect: '/login' })
);

server.get('/successLogin',
  (req, res) => {
    const CONTENT_DIR = join(__dirname, './content');
    res.sendFile(join(CONTENT_DIR + '/successLogin.html'));
  }
);

server.get('/successLoginLocal',
  (req, res) => {
    res.json({ success: true });
  }
);

//
// Register API middleware
// -----------------------------------------------------------------------------

server.use('/api', router);

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
server.get('*', async (req, res, next) => {
  try {
    let statusCode = 200;
    const data = { title: '', description: '', css: '', body: '', entry: assets.main.js };
    const css = [];
    const context = {
      insertCss: styles => css.push(styles._getCss()),
      onSetTitle: value => (data.title = value),
      onSetMeta: (key, value) => (data[key] = value),
      onPageNotFound: () => (statusCode = 404),
      user: req.user?req.user:''
    };

    await Router.dispatch({ path: req.path, query: req.query, context }, (state, component) => {
      data.body = ReactDOM.renderToString(component);
      data.css = css.join('');
    });

    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);
    res.status(statusCode).send(`<!doctype html>\n${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Launch the server
// -----------------------------------------------------------------------------
server.listen(port, () => {
  /* eslint-disable no-console */
  console.log(`The server is running at http://localhost:${port}/`);
});
