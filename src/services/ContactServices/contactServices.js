import Services from '../Services.js';

let instance;

class ContactServices {
  constructor() {
    if (!instance) {
      instance = this;
    }
    this.query = `/api/contact`;
    this.options = {
    };
    return instance;
  }

  async contact(message) {
    const body = JSON.stringify(message);
    const options = {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: body,
      credentials: 'include'

    };
    let response = await fetch(this.query, options);
    let data = await response.json();
    return data;
  }
  async contactCar(message, id) {
    const body = JSON.stringify({ message: message, id: id });
    const options = {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: body,
      credentials: 'include'

    };
    let response = await fetch(`/api/contact/car`, options);
    let data = await response.json();
    return data;
  }
}

var contactServices = new ContactServices();

console.log(contactServices);
export default contactServices;