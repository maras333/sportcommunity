import Services from '../Services.js';
import {setGetParameter} from '../../core/utils';
import fetch from '../../core/fetch';

let instance;

class EventServices {
  constructor() {
    if (!instance) {
      instance = this;
    }

    this.query = `http://enduhub.com/pl/api/search/`;

    this.options = {};

    this.options_find = {};

    this.cache = false;

    return instance;

  }

  async getEventsToUser() {
    this.options = {
      method: "get",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token 3cab32e82ad450d54f70126d1fddfe4ea5645d10',
        // 'Access-Control-Allow-Origin': '*',
      },
      mode: 'no-cors',      // credentials: 'include',
    };
    let response = await fetch(`http://enduhub.com/pl/api/search/?name=monika+Czyz`, this.options);
    let data = await response.json();
    return data;
  }

  async edit(user) {
    var data = JSON.stringify(user);
    this.options = {
      method: "put",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: data,
      credentials: 'include',
    };
    let response = await fetch(`/api/user/${user.id}`, this.options);
    let message = await response.json();
    return message;
  }
}

let event = new EventServices();

export default event;
