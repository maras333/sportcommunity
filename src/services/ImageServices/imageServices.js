import Services from '../Services.js';
import {setGetParameter} from '../../core/utils';
import fetch from '../../core/fetch';
import _ from 'lodash';

let instance;

class ImageServices {
  constructor() {
    if (!instance) {
      instance = this;
    }

    this.query = `/api/images/`;
    this.options = {};
    this.cache = false;

    return instance;
  }

  async create(image) {
    this.options = {
      method: "post",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: image,
      credentials: 'include',
    };
    let result = await fetch(this.query, this.options);
    return result;
  }

  async update(image, userId) {

    var img = JSON.stringify(image);
        console.log('image service update here');
    this.options = {
      method: "put",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: img,
      credentials: 'include',

    };

    let result = await fetch( `/api/image/${image.id}`, this.options);

    return result;
  }

  async list() {
    this.options = {
      method: "get",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    };
    let result = await fetch(this.query, this.options);
    var msg = result.json();
    return msg;
  }


}

let image = new ImageServices();

export default image;
