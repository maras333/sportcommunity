/** 
 * Services master class
 * 
 * Singleton
 * 
 **/
let instance = null;
class Services {
  constructor() {
    if (!instance) {
      instance = this;
    }
    return instance;
  }
  async get() {
    return [];
  }
  async post() {
    return [];
  }
  async put() {
    return [];
  }
  async delete() {
    return [];
  }
}

export default Services;