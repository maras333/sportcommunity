import Services from '../Services.js';
import {setGetParameter} from '../../core/utils';
import fetch from '../../core/fetch';

let instance;

class UserServices {
  constructor() {
    if (!instance) {
      instance = this;
    }

    this.query = `/api/user`;

    this.options = {};

    this.options_find = {};

    this.cache = false;

    return instance;

  }

  async get(id) {
    this.options = {
      method: "get",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    };
    let response = await fetch(`/api/user/${id}`, this.options);
    let data = await response.json();
    return data;
  }

  async getList() {
    this.options = {
      method: "get",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    };
    let response = await fetch(`/api/users`, this.options);
    let data = await response.json();
    return data;
  }

  async edit(user) {
    var data = JSON.stringify(user);
    this.options = {
      method: "put",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: data,
      credentials: 'include',
    };
    let response = await fetch(`/api/user/${user.id}`, this.options);
    let message = await response.json();
    return message;
  }

  async setLikedPhoto(imageId) {
    this.options = {
      method: "post",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
    };
    let result = await fetch(`/api/users/likePhoto/${imageId}`, this.options);
    let data = await result.json();
    return data;
  }

  async removeLikedPhoto(imageId) {
    this.options = {
      method: "delete",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
    };
    let result = await fetch(`/api/users/likePhoto/${imageId}`, this.options);
    let data = await result.json();
    return data;
  }

  async getLikedPhotos() {
    this.options = {
      method: "get",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
    };
    let result = await fetch(`/api/users/likedPhotos/`, this.options);
    let data = await result.json();
    return data;
  }

  async removeImage(imageId) {
    this.options = {
      method: "delete",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
    };
    let result = await fetch(`/api/user/images/${imageId}`, this.options);
    let message = await result.json();
    return message;
  }
}

let user = new UserServices();

export default user;
